﻿using FakturaMvc.Mapper;
using FakturaMvc.Models;
using FakturaMvc.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FakturaMvc.Controllers
{
    public class FakturaController : Controller
    {
        // GET: Faktura
        public ActionResult Faktura(int id)
        {
            var fakturaService = new FakturaService();
            var faktura = fakturaService.getById(id);



            ViewBag.messages = fakturaService.messages;
                   
            //var faktura = new Faktura();
            return View(faktura);


            //var fakturaDoWidoku = listaFaktur.FirstOrDefault(f => f.idFaktura == id);

            /* Faktura fakDoEdycji = null;

             foreach (var faktura in listaFaktur)
             {
                 if (faktura.idFaktura==id)
                 {
                     fakDoEdycji = faktura;
                 }

             }*/



        }

        public ActionResult ListaFaktur()
        {
            var fakturaService = new FakturaService();
            var listaFaktur = fakturaService.getList();
            return View(listaFaktur);
        }

        /*public ActionResult Edit(int id)
        {
            var fakturaMapper = new FakturaMapper();
            var nowaFaktura = fakturaMapper.getById(0);
            //var faktura = new Faktura();
            return View(nowaFaktura);
        }*/

        /*public ActionResult EdycjaFaktura()
        {
            var fakturaMapper = new FakturaMapper();
            var edycjaFaktura = fakturaMapper.getById();
            //var faktura = new Faktura();
            return View(edycjaFaktura);

        }*/

        /*public ActionResult NowaFaktura()
        {
            return View();
        }*/

       

        public ActionResult FakturaDoEdycji(int id)
        {
            Faktura fakturaDoEdycji;
            var fakturaService = FakturaService.getSingleton();

            if (id == 0)
            {
                fakturaDoEdycji = new Faktura();
            }
            else
            {
                fakturaDoEdycji = fakturaService.getById(id);
                
            }
            ViewBag.messages = fakturaService.messages;
            fakturaService.messages = new List<Message>();
            
            return View(fakturaDoEdycji);
        }



        [HttpPost]
        public ActionResult FakturaDoEdycji(Faktura fakturaDoEdycji)
        {
            var fakturaService = FakturaService.getSingleton();
            fakturaService.save(fakturaDoEdycji);           
            return Redirect($"/Faktura/Faktura/{fakturaDoEdycji.idFaktura}");
        }



        public ActionResult UsunFaktura(int id)
        {
            Faktura usunFaktura;
            var fakturaService = FakturaService.getSingleton();
            usunFaktura = fakturaService.getById(id);
            var wynik = fakturaService.delete(usunFaktura);

            //return Content($"Wynik usunięcia {wynik}");
            if (wynik == true)
            {
                return Redirect($"/Faktura/ListaFaktur");

            }
            else
            {
                return Content($"Problem z usunięciem faktury.");
            }
        }        
    }
}