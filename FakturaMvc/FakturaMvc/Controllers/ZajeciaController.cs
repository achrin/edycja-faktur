﻿using FakturaMvc.Mapper;
using FakturaMvc.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FakturaMvc.Controllers
{
    public class ZajeciaController : Controller
    {
        // GET: Zajecia
        public ActionResult Zajecia(int id)
        {
            var zajeciaMapper = new ZajeciaMapper();
            var zajecia = zajeciaMapper.getById(id);
            return View(zajecia);
        }
        public ActionResult ListaZajec()
        {
            var zajeciaMapper = new ZajeciaMapper();
            var listaZajec = zajeciaMapper.getList();
            return View(listaZajec);
        }
        public ActionResult ZajeciaDoEdycji(int id)
        {
            Zajecia zajeciaDoEdycji;
            var zajeciaMapper = new ZajeciaMapper();
            if (id == 0)
            {
                zajeciaDoEdycji = new Zajecia();
                
            }
            else
            {
                zajeciaDoEdycji = zajeciaMapper.getById(id);
            }
            return View(zajeciaDoEdycji);
        }
        [HttpPost]
        public ActionResult ZajeciaDoEdycji(Zajecia zajeciaDoEdycji)
        {
            var zajeciaMapper = new ZajeciaMapper();
            zajeciaMapper.save(zajeciaDoEdycji);
            return Content($"Zajęcia numer {zajeciaDoEdycji.id} została zapisana poprawnie.");
        }
        public ActionResult UsunZajecia(int id)
        {
            Zajecia usunZajecia;
            var zajeciaMapper = new ZajeciaMapper();
            usunZajecia = zajeciaMapper.getById(id);
            var wynik = zajeciaMapper.delete(usunZajecia);
            if(wynik == true)
            {
                return Content($"Pomyślnie usunięto zajecia.");
            }
            else
            {
                return Content($"Problem z usunięciem zajecia.");
            }
        }
    }
}