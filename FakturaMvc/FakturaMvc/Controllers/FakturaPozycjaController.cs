﻿using FakturaMvc.Models;
using FakturaMvc.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FakturaMvc.Controllers
{
    public class FakturaPozycjaController : Controller
    {
        //GET: Faktura
        public ActionResult Pozycja (int id)
        {
            var fakturaPozycjaService = new FakturaService();
            var pozycja = fakturaPozycjaService.getPozycjaById(id);
            ViewBag.messages = fakturaPozycjaService.messages;
            return View(pozycja);
        }


        public ActionResult ListaPozycji(int id_pozycja)
        {
            var fakturaPozycjaService = new FakturaService();
            var listaPozycja = fakturaPozycjaService.getPozycjaList(id_pozycja);
            return View(listaPozycja);
        }


        public ActionResult PozycjaDoEdycji(int id, int id2)
        {
            FakturaPozycja pozycjaDoEdycji;
            var fakturaPozycjaService = FakturaService.getSingleton();

            if (id == 0)
            {
                pozycjaDoEdycji = new FakturaPozycja();
            }
            else
            {
                pozycjaDoEdycji = fakturaPozycjaService.getPozycjaById(id2);
            }

            pozycjaDoEdycji.id_faktura = id;
            ViewBag.messages = fakturaPozycjaService.messages;

            return View(pozycjaDoEdycji);
        }
        [HttpPost]
        public ActionResult PozycjaDoEdycji(FakturaPozycja pozycjaDoEdycji)
        {
            var fakturaPozycjaService = FakturaService.getSingleton();
            fakturaPozycjaService.save(pozycjaDoEdycji);
            return Redirect($"/Faktura/Faktura/{pozycjaDoEdycji.id_faktura}");
        }
        public ActionResult UsunPozycja(int lp)
        {
            FakturaPozycja usunPozycja;
            var fakturaPozycjaService = FakturaService.getSingleton();
            usunPozycja = fakturaPozycjaService.getPozycjaById(lp);
            var wynik = fakturaPozycjaService.delete(usunPozycja);

            //return Content($"Wynik usunięcia {wynik}");
            if (wynik == true)
            {
                return Content($"Pomyślnie usunięto pozycję.");

            }
            else
            {
                return Content($"Problem z usunięciem pozycji.");
            }
        }
    }
}