﻿using FakturaMvc.Mapper;
using FakturaMvc.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FakturaMvc.Controllers
{
    public class KsiazkaController : Controller
    {
        // GET: Ksiazka
        public ActionResult Ksiazka(int id)
        {
            var ksiazkaMapper = new KsiazkaMapper(); //dodać using ctrl+"."
            var ksiazka = ksiazkaMapper.getById(id);
            return View(ksiazka);
        }
        public ActionResult ListaKsiazek()
        {
            var ksiazkaMapper = new KsiazkaMapper();
            var listaKsiazek = ksiazkaMapper.getList();
            return View(listaKsiazek);
        }
        public ActionResult KsiazkaDoEdycji(int id)
        {
            Ksiazka ksiazkaDoEdycji;
            var ksiazkaMapper = new KsiazkaMapper();

            if (id == 0)
            {
                ksiazkaDoEdycji = new Ksiazka();
            }
            else
            {
                ksiazkaDoEdycji = ksiazkaMapper.getById(id);
            }

            return View(ksiazkaDoEdycji);
        }
        [HttpPost]
        public ActionResult KsiazkaDoEdycji(Ksiazka ksiazkaDoEdycji)
        {
            var ksiazkaMapper = new KsiazkaMapper();
            ksiazkaMapper.save(ksiazkaDoEdycji);
            return Content($"Książka nr {ksiazkaDoEdycji.id} została zapisana poprawnie");
        }
        public ActionResult UsunKsiazka(int id)
        {
            Ksiazka usunKsiazka;
            var ksiazkaMapper = new KsiazkaMapper();
            usunKsiazka = ksiazkaMapper.getById(id);
            var wynik = ksiazkaMapper.delete(usunKsiazka);

            //return Content($"Wynik usunięcia {wynik}");
            if (wynik == true)
            {
                return Content($"Pomyślnie usunięto książkę.");
            }
            else
            {
                return Content($"Problem z usunięciem książki.");
            }
        }
    }
}