﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace FakturaMvc
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Faktura", action = "ListaFaktur", id=UrlParameter.Optional}  //routa DOMYŚLNA!!!! (jeżeli nie wpiszę nic to bedzie lista faktur)
            );
            routes.MapRoute(
                name: "Default2",
                url: "{controller}/{action}/{id}/{id2}"
                //defaults: new { controller = "Faktura", action = "ListaFaktur", id = UrlParameter.Optional }  //routa DOMYŚLNA!!!! (jeżeli nie wpiszę nic to bedzie lista faktur)
            );
        }
    }
}
