﻿using FakturaMvc.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Web;

namespace FakturaMvc.Mapper
{
    public class FakturaPozycjaMapper
    {
        public FakturaPozycja getPozycjaById(int id)
        {
            
            var fakturaPozycja = new FakturaPozycja();
            using (var conn = new SqlConnection())
            {
                conn.ConnectionString = "Data Source = 192.168.10.248; Initial Catalog = Agata; User ID = agata; Password = pbs1234";
                conn.Open();
                using (var command = new SqlCommand())
                {
                    command.Connection = conn;
                    command.CommandText = $"select lp, nazwa, kwota_netto, stawka_vat, id_faktury from fakt_pozycja where lp={id}";
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (!reader.HasRows)
                        {
                            throw new Exception($"Brak pozycji o danym numerze {id}");
                        }
                        while (reader.Read())
                        {
                            
                            fakturaPozycja.lp = (int)reader["lp"];
                            fakturaPozycja.nazwa = (string)reader["nazwa"];
                            fakturaPozycja.kwotaNetto = (decimal)reader["kwota_netto"];
                            fakturaPozycja.stawkaVat = (int)reader["stawka_vat"];
                            fakturaPozycja.id_faktura = (int)reader["id_faktury"];
                        }
                    }
                }
                
            }
            return fakturaPozycja;
        }
        public List<FakturaPozycja> getPozycjaList(int id_faktura)
        {
            var listaPozycja = new List<FakturaPozycja>();
            using (var conn = new SqlConnection())
            {
                conn.ConnectionString = "Data Source = 192.168.10.248; Initial Catalog = Agata; User ID = agata; Password = pbs1234";
                conn.Open();
                using (var command = new SqlCommand())
                {
                    command.Connection = conn;
                    command.CommandText = $"select * from fakt_pozycja where id_faktury = {id_faktura}";
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        
                        while (reader.Read())
                        {
                            listaPozycja.Add(new FakturaPozycja()
                            {
                                lp = (int)reader["lp"],
                                nazwa = (string)reader["nazwa"],
                                kwotaNetto = (decimal)reader["kwota_netto"],
                                stawkaVat = (int)reader["stawka_vat"],
                                id_faktura = (int)reader["id_faktury"],
                            });
                        }
                    }
                }
            }
            return listaPozycja;
        }

        public FakturaPozycja save (FakturaPozycja fakturaPozycja)
        {
            using (var conn = new SqlConnection())
            {
                conn.ConnectionString = "Data Source = 192.168.10.248; Initial Catalog = Agata; User ID = agata; Password = pbs1234";
                conn.Open();
                using (var command = new SqlCommand())
                {
                    command.Connection = conn;
                    if (fakturaPozycja.lp == 0)
                    {
                        command.CommandText = $"insert into fakt_pozycja (nazwa, kwota_netto, stawka_vat, id_faktury) values ('{fakturaPozycja.nazwa}', {fakturaPozycja.kwotaNetto.ToString("G", CultureInfo.CreateSpecificCulture("en-US"))}, {fakturaPozycja.stawkaVat}, {fakturaPozycja.id_faktura}); select SCOPE_IDENTITY();";
                        fakturaPozycja.lp = Convert.ToInt32(command.ExecuteScalar());
                    }
                    else
                    {
                        command.CommandText = $"update fakt_pozycja set nazwa = '{fakturaPozycja.nazwa}', kwota_netto = {fakturaPozycja.kwotaNetto.ToString("G", CultureInfo.CreateSpecificCulture("en-US"))}, stawka_vat = {fakturaPozycja.stawkaVat}, id_faktury = {fakturaPozycja.id_faktura} where lp={fakturaPozycja.lp}";
                        command.ExecuteNonQuery();
                    }
                }
            }
            return fakturaPozycja;
        }

        public bool delete(FakturaPozycja fakturaPozycja)
        {
            using (var conn = new SqlConnection())
            {
                conn.ConnectionString = "Data Source = 192.168.10.248; Initial Catalog = Agata; User ID = agata; Password = pbs1234";
                conn.Open();
                using (var command = new SqlCommand())
                {
                    command.Connection = conn;
                    command.CommandText = $"delete from fakt_pozycja where lp={fakturaPozycja.lp}";
                    var iloscTabel = command.ExecuteNonQuery();
                    if (iloscTabel == 1)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
        }
    }
}