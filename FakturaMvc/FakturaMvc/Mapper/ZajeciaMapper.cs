﻿using FakturaMvc.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace FakturaMvc.Mapper
{
    public class ZajeciaMapper
    {
        /*public Zajecia getById(int id)
        {
            var listaZajec = new List<Zajecia>();
            var zajecia1 = new Zajecia();

            zajecia1.id = 1;
            zajecia1.nazwa = "prog";
            zajecia1.typ = "abc";
            zajecia1.miejsce = "Sopot";
            zajecia1.prowadzacy = "Jan Kowalski";

            listaZajec.Add(zajecia1);

            var zajecia2 = new Zajecia();

            zajecia2.id = 2;
            zajecia2.nazwa = "profs";
            zajecia2.typ = "afs";
            zajecia2.miejsce = "Sopot";
            zajecia2.prowadzacy = "Jan Kowalski";

            listaZajec.Add(zajecia2);

            Zajecia zajeciaDoEdycji = null;
            foreach (var zajecia in listaZajec)
            {
                if (zajecia.id == id)
                {
                    zajeciaDoEdycji = zajecia;
                }
                
            }
            return zajeciaDoEdycji;
        }

        public List<Zajecia> getList()
        {
            var listaZajec = new List<Zajecia>();
            var zajecia1 = new Zajecia();

            zajecia1.id = 1;
            zajecia1.nazwa = "prog";
            zajecia1.typ = "abc";
            zajecia1.miejsce = "Sopot";
            zajecia1.prowadzacy = "Jan Kowalski";

            listaZajec.Add(zajecia1);

            var zajecia2 = new Zajecia();

            zajecia2.id = 2;
            zajecia2.nazwa = "profs";
            zajecia2.typ = "afs";
            zajecia2.miejsce = "Sopot";
            zajecia2.prowadzacy = "Jan Kowalski";

            listaZajec.Add(zajecia2);

            return listaZajec;
        }*/
        public List<Zajecia> getList()
        {
            var listaZajec = new List<Zajecia>();
            using (var conn = new SqlConnection())
            {
                conn.ConnectionString = "Data Source = 192.168.10.248; Initial Catalog = Agata; User ID = agata; Password = pbs1234";
                conn.Open();
                using (var command = new SqlCommand())
                {
                    command.Connection = conn;
                    command.CommandText = "select id_zajecia, nazwa, typ, miejsce, prowadzący from fakt_zajecia";
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            listaZajec.Add(new Zajecia()
                            {
                                id = (int)reader["id_zajecia"],
                                nazwa = (string)reader["nazwa"],
                                typ = (string)reader["typ"],
                                miejsce = (string)reader["miejsce"],
                                prowadzacy = (string)reader["prowadzący"]
                            });
                        }
                    }
                }
            }
            return listaZajec;
        }
        public Zajecia getById(int id)
        {
            var zajecia = new Zajecia();
            using (var conn = new SqlConnection())
            {
                conn.ConnectionString = "Data Source = 192.168.10.248; Initial Catalog = Agata; User ID = agata; Password = pbs1234";
                conn.Open();
                using (var command = new SqlCommand())
                {
                    command.Connection = conn;
                    command.CommandText = $"select id_zajecia, nazwa, typ, miejsce, prowadzący from fakt_zajecia where id_zajecia = {id}";

                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            zajecia.id = (int)reader["id_zajecia"];
                            zajecia.nazwa = (string)reader["nazwa"];
                            zajecia.typ = (string)reader["typ"];
                            zajecia.miejsce = (string)reader["miejsce"];
                            zajecia.prowadzacy = (string)reader["prowadzący"];
                        }
                    }
                }
            }           
            return zajecia;
        }


        public Zajecia save(Zajecia zajecia)
        {
            using (var conn = new SqlConnection())
            {
                conn.ConnectionString = "Data Source = 192.168.10.248; Initial Catalog = Agata; User ID = agata; Password = pbs1234";
                conn.Open();
                using (var command = new SqlCommand())
                {
                    command.Connection = conn;
                    if(zajecia.id ==0)
                    {
                        command.CommandText = $"insert into fakt_zajecia (nazwa, typ, miejsce, prowadzący) values ('{zajecia.nazwa}', '{zajecia.typ}', '{zajecia.miejsce}', '{zajecia.prowadzacy}'); select scope_identity();";
                        zajecia.id = Convert.ToInt32(command.ExecuteScalar());
                    }
                    else
                    {
                        command.CommandText = $"update fakt_zajecia set nazwa = '{zajecia.nazwa}', typ = '{zajecia.typ}', miejsce = '{zajecia.miejsce}', prowadzący = '{zajecia.prowadzacy}' where id_zajecia = {zajecia.id}";
                        command.ExecuteNonQuery();
                    }
                }
            }

            return zajecia;
        }

        public bool delete(Zajecia zajecia)
        {
            using (var conn = new SqlConnection())
            {
                conn.ConnectionString = $"Data Source = 192.168.10.248; Initial Catalog = Agata; User ID = agata; Password = pbs1234";
                conn.Open();
                using (var command = new SqlCommand())
                {
                    command.Connection = conn;
                    command.CommandText = $"delete from fakt_zajecia where id_zajecia = {zajecia.id}";
                    var iloscTabel = command.ExecuteNonQuery();
                    if (iloscTabel == 1)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
        }
    }
}