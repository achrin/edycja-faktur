﻿using FakturaMvc.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;

namespace FakturaMvc.Mapper
{
    public class KsiazkaMapper
    {
        /*public Ksiazka getById(int id)
        {
            var listaKsiazek = new List<Ksiazka>();

            var ksiazka1 = new Ksiazka();

            ksiazka1.id = 1;
            ksiazka1.tytul = "Nad Niemnem";
            ksiazka1.isbn = 9788354321721;
            ksiazka1.cena = 22;
            ksiazka1.autor = "Eliza Orzeszkowa";
            Console.WriteLine(ksiazka1.pobierzOpis());



            var ksiazka2 = new Ksiazka();
            ksiazka2.id = 2;
            ksiazka2.tytul = "Pan Tadeusz";
            ksiazka2.isbn = 9788354321725;
            ksiazka2.cena = 42.50;
            ksiazka2.autor = "Adam Mickiewicz";
            Console.WriteLine(ksiazka2.pobierzOpis());



            listaKsiazek.Add(ksiazka1);
            listaKsiazek.Add(ksiazka2);



            Ksiazka ksiazkaDoEdycji = null;

            foreach (var ksiazka in listaKsiazek)
            {
                if (ksiazka.id == id)
                {
                    ksiazkaDoEdycji = ksiazka;
                }

            }
            return ksiazkaDoEdycji;
        }
        public List<Ksiazka> getList()
        {
            var listaKsiazek = new List<Ksiazka>();

            var ksiazka1 = new Ksiazka();

            ksiazka1.id = 1;
            ksiazka1.tytul = "Nad Niemnem";
            ksiazka1.isbn = 9788354321721;
            ksiazka1.cena = 22;
            ksiazka1.autor = "Eliza Orzeszkowa";
            Console.WriteLine(ksiazka1.pobierzOpis());



            var ksiazka2 = new Ksiazka();
            ksiazka2.id = 2;
            ksiazka2.tytul = "Pan Tadeusz";
            ksiazka2.isbn = 9788354321725;
            ksiazka2.cena = 42.50;
            ksiazka2.autor = "Adam Mickiewicz";
            Console.WriteLine(ksiazka2.pobierzOpis());



            listaKsiazek.Add(ksiazka1);
            listaKsiazek.Add(ksiazka2);
            return listaKsiazek;        
        }*/
        public List<Ksiazka> getList()
        {
            var listaKsiazek = new List<Ksiazka>();
            using (var conn = new SqlConnection())
            {

                conn.ConnectionString = "Data Source = 192.168.10.248; Initial Catalog = Agata; User ID = agata; Password = pbs1234";
                conn.Open();
                using (var command = new SqlCommand())
                {
                    command.Connection = conn;
                    command.CommandText = "select id_ksiazka, tytul from fakt_ksiazka";
                    using (SqlDataReader read = command.ExecuteReader())
                    {
                        while (read.Read())
                        {
                            listaKsiazek.Add(new Ksiazka()
                            {
                                id = (int)read["id_ksiazka"],
                                tytul = (string)read["tytul"]
                            });
                        }
                        return listaKsiazek;
                    }
                }
            }
        }
        public Ksiazka getById(int id)
        {
            var ksiazka = new Ksiazka();
            using (var conn = new SqlConnection())
            {
                conn.ConnectionString = "Data Source = 192.168.10.248; Initial Catalog = Agata; User ID = agata; Password = pbs1234";
                conn.Open();
                using (var command = new SqlCommand())
                {
                    command.Connection = conn;
                    command.CommandText = $"select id_ksiazka, tytul, numer_isbn, cena, autor from fakt_ksiazka where id_ksiazka={id}";
                    using (var read = command.ExecuteReader())
                    {
                        while (read.Read())
                        {
                            ksiazka.id = (int)read["id_ksiazka"];
                            ksiazka.tytul = (string)read["tytul"];
                            ksiazka.isbn = (long)read["numer_isbn"];
                            ksiazka.cena = (decimal)read["cena"];
                            ksiazka.autor = (string)read["autor"];
                        }
                        return ksiazka;
                    }
                }               
            }
        }
        public Ksiazka save(Ksiazka ksiazka)
        {
            using (var conn = new SqlConnection())
            {
                conn.ConnectionString = "Data Source = 192.168.10.248; Initial Catalog = Agata; User ID = agata; Password = pbs1234";
                conn.Open();
                using (var command = new SqlCommand())
                {
                    command.Connection = conn;
                    if (ksiazka.id == 0)
                    {
                        command.CommandText = $"insert into fakt_ksiazka (tytul, numer_isbn, autor, cena) values ('{ksiazka.tytul}', {ksiazka.isbn}, '{ksiazka.autor}', {ksiazka.cena}); select SCOPE_IDENTITY();";
                        ksiazka.id = Convert.ToInt32(command.ExecuteScalar());
                    }
                    else
                    {
                        command.CommandText = $"update fakt_ksiazka set tytul = '{ksiazka.tytul}', numer_isbn = {ksiazka.isbn}, autor = '{ksiazka.autor}' where id_ksiazka={ksiazka.id}";
                        command.ExecuteNonQuery();
                    }
                }
            }
            return ksiazka;
        }
        public bool delete(Ksiazka ksiazka)
        {
            using (var conn = new SqlConnection())
            {
                conn.ConnectionString = "Data Source = 192.168.10.248; Initial Catalog = Agata; User ID = agata; Password = pbs1234";
                conn.Open();
                using (var command = new SqlCommand())
                {
                    command.Connection = conn;
                    command.CommandText = $"delete from fakt_ksiazka where id_ksiazka={ksiazka.id}";
                    var iloscTabel = command.ExecuteNonQuery();
                    if (iloscTabel == 1)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
        }
    }
}