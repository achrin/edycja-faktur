﻿using FakturaMvc.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Windows.Forms;

namespace FakturaMvc.Mapper
{
    public class FakturaMapper
    {
        /*public Faktura getById(int id)
        {
            var listaFaktur = new List<Faktura>();

            var faktura1 = new Faktura();
            faktura1.idFaktura = 1;
            faktura1.dostawca = "Agata";
            faktura1.odbiorca = "Jan Kowalski";
            faktura1.numerNip = 4443321456532;

            var fakturaPozycja1 = new FakturaPozycja();
            fakturaPozycja1.lp = 1;
            fakturaPozycja1.nazwa = "długopis";
            fakturaPozycja1.kwotaNetto = 22.34M;
            fakturaPozycja1.stawkaVat = 23;

            var fakturaPozycja2 = new FakturaPozycja();
            fakturaPozycja2.lp = 2;
            fakturaPozycja2.nazwa = "ołówek";
            fakturaPozycja2.kwotaNetto = 12.43M;
            fakturaPozycja2.stawkaVat = 23;

            faktura1.pozycje.Add(fakturaPozycja1);
            faktura1.pozycje.Add(fakturaPozycja2);

            listaFaktur.Add(faktura1);

            var faktura2 = new Faktura();
            faktura2.idFaktura = 2;
            faktura2.dostawca = "Ewa";
            faktura2.odbiorca = "Jan";
            faktura2.numerNip = 4443321454357;

            var fakturaPozycja3 = new FakturaPozycja();
            fakturaPozycja3.lp = 1;
            fakturaPozycja3.nazwa = "długopis";
            fakturaPozycja3.kwotaNetto = 18.34M;
            fakturaPozycja3.stawkaVat = 23;

            var fakturaPozycja4 = new FakturaPozycja();
            fakturaPozycja4.lp = 2;
            fakturaPozycja4.nazwa = "ołówek";
            fakturaPozycja4.kwotaNetto = 2.43M;
            fakturaPozycja4.stawkaVat = 23;

            faktura2.pozycje.Add(fakturaPozycja3);
            faktura2.pozycje.Add(fakturaPozycja4);

            listaFaktur.Add(faktura2);

            var faktura3 = new Faktura();
            faktura3.idFaktura = 3;
            faktura3.dostawca = "Rafał";
            faktura3.odbiorca = "Maciej";
            faktura3.numerNip = 4443432454357;

            var fakturaPozycja5 = new FakturaPozycja();
            fakturaPozycja5.lp = 1;
            fakturaPozycja5.nazwa = "długopis";
            fakturaPozycja5.kwotaNetto = 2.34M;
            fakturaPozycja5.stawkaVat = 23;

            var fakturaPozycja6 = new FakturaPozycja();
            fakturaPozycja6.lp = 2;
            fakturaPozycja6.nazwa = "ołówek";
            fakturaPozycja6.kwotaNetto = 1.43M;
            fakturaPozycja6.stawkaVat = 23;

            faktura3.pozycje.Add(fakturaPozycja5);
            faktura3.pozycje.Add(fakturaPozycja6);

            listaFaktur.Add(faktura3);


            Faktura fakDoEdycji = null;

            foreach (var faktura in listaFaktur)
            {
                if (faktura.idFaktura == id)
                {
                    fakDoEdycji = faktura;
                }

            }
            return fakDoEdycji;
        }

        public List<Faktura> getList()
        {
            var listaFaktur = new List<Faktura>();

            var faktura1 = new Faktura();
            faktura1.idFaktura = 1;
            faktura1.dostawca = "Agata";
            faktura1.odbiorca = "Jan Kowalski";
            faktura1.numerNip = 4443321456532;

            var fakturaPozycja1 = new FakturaPozycja();
            fakturaPozycja1.lp = 1;
            fakturaPozycja1.nazwa = "długopis";
            fakturaPozycja1.kwotaNetto = 22.34M;
            fakturaPozycja1.stawkaVat = 23;

            var fakturaPozycja2 = new FakturaPozycja();
            fakturaPozycja2.lp = 2;
            fakturaPozycja2.nazwa = "ołówek";
            fakturaPozycja2.kwotaNetto = 12.43M;
            fakturaPozycja2.stawkaVat = 23;

            faktura1.pozycje.Add(fakturaPozycja1);
            faktura1.pozycje.Add(fakturaPozycja2);

            listaFaktur.Add(faktura1);

            var faktura2 = new Faktura();
            faktura2.idFaktura = 2;
            faktura2.dostawca = "Ewa";
            faktura2.odbiorca = "Jan";
            faktura2.numerNip = 4443321454357;

            var fakturaPozycja3 = new FakturaPozycja();
            fakturaPozycja3.lp = 1;
            fakturaPozycja3.nazwa = "długopis";
            fakturaPozycja3.kwotaNetto = 18.34M;
            fakturaPozycja3.stawkaVat = 23;

            var fakturaPozycja4 = new FakturaPozycja();
            fakturaPozycja4.lp = 2;
            fakturaPozycja4.nazwa = "ołówek";
            fakturaPozycja4.kwotaNetto = 2.43M;
            fakturaPozycja4.stawkaVat = 23;

            faktura2.pozycje.Add(fakturaPozycja3);
            faktura2.pozycje.Add(fakturaPozycja4);

            listaFaktur.Add(faktura2);

            var faktura3 = new Faktura();
            faktura3.idFaktura = 3;
            faktura3.dostawca = "Rafał";
            faktura3.odbiorca = "Maciej";
            faktura3.numerNip = 4443432454357;

            var fakturaPozycja5 = new FakturaPozycja();
            fakturaPozycja5.lp = 1;
            fakturaPozycja5.nazwa = "długopis";
            fakturaPozycja5.kwotaNetto = 2.34M;
            fakturaPozycja5.stawkaVat = 23;

            var fakturaPozycja6 = new FakturaPozycja();
            fakturaPozycja6.lp = 2;
            fakturaPozycja6.nazwa = "ołówek";
            fakturaPozycja6.kwotaNetto = 1.43M;
            fakturaPozycja6.stawkaVat = 23;

            faktura3.pozycje.Add(fakturaPozycja5);
            faktura3.pozycje.Add(fakturaPozycja6);

            listaFaktur.Add(faktura3);
            return listaFaktur;
        }*/



        public List<Faktura> getList()
        {
            var listaFaktur = new List<Faktura>();            
            using (var conn = new SqlConnection())
            {
                conn.ConnectionString = "Data Source = 192.168.10.248; Initial Catalog = Agata; User ID = agata; Password = pbs1234";
                conn.Open();
                using (var command = new SqlCommand())
                {
                    command.Connection = conn;
                    command.CommandText = "select * from fakt_faktura";
                    using (SqlDataReader reader = command.ExecuteReader())
                    {                        
                       while (reader.Read())
                        {
                            listaFaktur.Add(new Faktura()
                            {
                                idFaktura = (int) reader["id_faktura"],
                                 dostawca = (string)reader["dostawca"],
                                 odbiorca = (string)reader["odbiorca"]                                 
                            });
                        }
                    }
                }
            }
            return listaFaktur;
        }

        public Faktura getById(int id)
        {            
            var faktura = new Faktura();
            using (var conn = new SqlConnection())
            {
                conn.ConnectionString = "Data Source = 192.168.10.248; Initial Catalog = Agata; User ID = agata; Password = pbs1234";
                conn.Open();
                using (var command = new SqlCommand())
                {
                    command.Connection = conn;
                    command.CommandText = $"select id_faktura, dostawca, odbiorca, nip, data_wystawienia from fakt_faktura where id_faktura = {id}";
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        //jeśli brak wierszy to wyjątek
                        if (!reader.HasRows)
                        {
                            throw new Exception($"Brak faktury o id = {id}");
                        }


                        while (reader.Read())
                        {
                            faktura.idFaktura = (int)reader["id_faktura"];
                            faktura.dostawca = (string)reader["dostawca"];
                            faktura.odbiorca = (string)reader["odbiorca"];
                            faktura.numerNip = (long)reader["nip"];                            
                        }                        
                    }
                }
            }
            
            return faktura;
        }

        public Faktura save (Faktura faktura)
        {

            if (faktura.obliczeniaNip() == false)
            {
                throw new Exception($"Błędny numer NIP");
            }

            using (var conn = new SqlConnection())
            {                
                conn.ConnectionString = "Data Source = 192.168.10.248; Initial Catalog = Agata; User ID = agata; Password = pbs1234";
                conn.Open();
                using (var command = new SqlCommand())
                {
                    command.Connection = conn;

                    if (faktura.idFaktura == 0)
                    {
                        command.CommandText = $"insert into fakt_faktura (dostawca, odbiorca, nip) values ('{faktura.dostawca}', '{faktura.odbiorca}', {faktura.numerNip}); select SCOPE_IDENTITY();";
                        faktura.idFaktura = Convert.ToInt32(command.ExecuteScalar());
                    }
                    else
                    {
                        command.CommandText = $"update fakt_faktura set dostawca = '{faktura.dostawca}', odbiorca = '{faktura.odbiorca}', nip = {faktura.numerNip} where id_faktura={faktura.idFaktura}";
                        //var rowsAffected = command.ExecuteNonQuery();
                        //if (rowsAffected ==0)
                        //{
                        //    throw new Exception("Brak faktury o danym id");
                        //}
                      
                        
                    }
                }
            }
            return faktura;
        }

        public bool delete(Faktura faktura)
        {
            using (var conn = new SqlConnection())
            {
                conn.ConnectionString = "Data Source = 192.168.10.248; Initial Catalog = Agata; User ID = agata; Password = pbs1234";
                conn.Open();
                using (var command = new SqlCommand())
                {
                    command.Connection = conn;
                    command.CommandText = $"delete from fakt_faktura where id_faktura={faktura.idFaktura}";
                    var iloscTabel = command.ExecuteNonQuery();
                    if(iloscTabel == 1)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }            
        }
    }
}