﻿using FakturaMvc.Mapper;
using FakturaMvc.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FakturaMvc.Service
{
    public class FakturaService
    {
        private static FakturaService fakturaService = null;

        public List<Message> messages = new List<Message>();

        public static FakturaService getSingleton()
        {
            if (fakturaService == null)
            {
                fakturaService = new FakturaService();
            }
            return fakturaService;
        }

        public Faktura getById(int id)
        {
            
                var fakturaMapper = new FakturaMapper();


            Faktura faktura = null;
            try
            {
                faktura = fakturaMapper.getById(id);
            }
            catch (Exception e)
            {
                var m = new Message();
                m.typ = Message.TYP_BLAD;
                m.opis = e.Message;
                messages.Add(m);
            }

            try
            {
                var fakturaPozycjaMapper = new FakturaPozycjaMapper();
                var listaPozycja = fakturaPozycjaMapper.getPozycjaList(id);

                faktura.pozycje = listaPozycja;
            } catch (Exception e)
            {

            }
                return faktura;
            
        }


        public List<Faktura> getList()
        {
            var fakturaMapper = new FakturaMapper();
            var listaFaktur = fakturaMapper.getList();
            return listaFaktur;
        }

        public Faktura save (Faktura faktura)
        {
            var fakturaMapper = new FakturaMapper();

            try
            {
                faktura = fakturaMapper.save(faktura);
            }
            catch (Exception e)
            {
                var m = new Message();
                m.typ = Message.TYP_BLAD;
                m.opis = e.Message;
                messages.Add(m);
            }
           
            return faktura;
        }

        public bool delete (Faktura faktura)
        {
            var fakturaMapper = new FakturaMapper();
            var usunFaktura = fakturaMapper.delete(faktura);
            return usunFaktura;
        }

        public FakturaPozycja getPozycjaById(int id)
        {
            var fakturaPozycjaMapper = new FakturaPozycjaMapper();
            FakturaPozycja pozycja = null;
            try
            {


                pozycja = fakturaPozycjaMapper.getPozycjaById(id);
            }
            catch (Exception f)
            {
                var n = new Message();
                n.typ = Message.TYP_BLAD;
                n.opis = f.Message;

                //n = null;

                
                messages.Add(n);
            }
            return pozycja;
        }

        public List<FakturaPozycja> getPozycjaList(int id_faktura)
        {
            var fakturaPozycjaMapper = new FakturaPozycjaMapper();
            var listaPozycja = fakturaPozycjaMapper.getPozycjaList(id_faktura);
            return listaPozycja;
        }

        public FakturaPozycja save (FakturaPozycja fakturaPozycja)
        {
            var fakturaPozycjaMapper = new FakturaPozycjaMapper();
            var zapiszPozycja = fakturaPozycjaMapper.save(fakturaPozycja);
            return zapiszPozycja;
        }

        public bool delete(FakturaPozycja fakturaPozycja)
        {
            var fakturaPozycjaMapper = new FakturaPozycjaMapper();
            var usunFaktura = fakturaPozycjaMapper.delete(fakturaPozycja);
            return usunFaktura;
        }
    }
}