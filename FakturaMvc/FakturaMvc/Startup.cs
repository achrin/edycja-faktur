﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(FakturaMvc.Startup))]
namespace FakturaMvc
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
