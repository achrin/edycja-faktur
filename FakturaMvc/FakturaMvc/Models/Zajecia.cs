﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FakturaMvc.Models
{
    public class Zajecia
    {
        public int id { get; set; } = 0;
        public string nazwa { get; set; } = string.Empty;
        public DateTime czasStartu { get; set; } = DateTime.MinValue;
        public string typ { get; set; } = string.Empty;
        public string miejsce { get; set; } = string.Empty;
        public DateTime czasKonca { get; set; } = DateTime.MinValue;
        public string prowadzacy { get; set; } = string.Empty;
        public double liczba1 { get; set; } = 0;
        public double liczba2 { get; set; } = 0;
        public string nazwaProjektu { get; set; } = string.Empty;
        public string pobierzOpis()
        {
            var opisZajecia = "Nazwa " + nazwa + ", czas startu " + czasStartu + ", typ " + typ + ", czas końca " + czasKonca
                + ", prowadzacy " + prowadzacy + ", trwanie " + czasTrwania() + ", wynik " + wynik() + ", nazwa projektu " + nazwaProjektu;
            if (czyWTrakcie() == true)

            {

                opisZajecia += ", w trakcie zajęć";
            }

            else

            {
                opisZajecia += ", poza zajęciami";
            }

            return opisZajecia;
        }
        public bool czyWTrakcie()
        {
            if (czasStartu.CompareTo(DateTime.Now) == -1)
            {
                if (czasKonca.CompareTo(DateTime.Now) == 1)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }

            else
            {
                return false;

            }
        }
        public string czasTrwania()
        {
            var godziny = (czasKonca - czasStartu).Hours;
            var minuty = (czasKonca - czasStartu).Minutes;
            var sekundy = (czasKonca - czasStartu).Seconds;
            var trwanie = godziny + ":" + minuty + ":" + sekundy;
            return trwanie;
        }

        public double wynik()

        {
            var wynik1 = liczba1 + liczba2;
            return wynik1;
        }
    }
}