﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FakturaMvc.Models
{
    public class FakturaPozycja
    {
       
            public int lp { get; set; } = 0;
            public string nazwa { get; set; } = string.Empty;
            public decimal kwotaNetto { get; set; } = 0;
            public int stawkaVat { get; set; } = 0;
        public int id_faktura { get; set; } = 0;

        public decimal kwotaVat { get
            {
                var suma = Math.Round((kwotaNetto * stawkaVat / 100),2);
                return suma;
            } }


        public decimal kwotaBrutto 
        {
            get
            {
                var suma = Math.Round(kwotaNetto + (kwotaNetto * stawkaVat / 100),2);
                return suma;
            }
        }
    }
}