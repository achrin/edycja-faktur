﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FakturaMvc.Models
{
    public class Faktura
    {
        public int idFaktura { get; set; } = 0;
        public string dostawca { get; set; } = string.Empty;
        public string odbiorca { get; set; } = string.Empty;
        public DateTime dataPlatnosci { get; set; } = DateTime.MinValue;
        public long numerNip { get; set; } = 0;
        public List<FakturaPozycja> pozycje { get; set; } = new List<FakturaPozycja>();

        public string pobierzOpis()
        {
            var opisFaktury = "╔══════════════════════════════════════════════════════════════════════════════╗\n";


            opisFaktury += "║" + String.Format("{0,-21}", " ID") + "= " + String.Format("{0,-55}", idFaktura) + "║\n";
            opisFaktury += "║" + String.Format("{0,-21}", " Dostawca") + "= " + String.Format("{0,-55}", dostawca) + "║\n";
            opisFaktury += "║" + String.Format("{0,-21}", " Odbiorca") + "= " + String.Format("{0,-55}", odbiorca) + "║\n";
            opisFaktury += "║" + String.Format("{0,-21}", " NIP") + "= " + String.Format("{0,-55}", numerNip) + "║\n";
            opisFaktury += "║" + String.Format("{0,-21}", " Data płatności") + "= " + String.Format("{0,-55}", dataPlatnosci) + "║\n";
            opisFaktury += "╠══════════════════════════════════════════════════════════════════════════════╣\n";
            opisFaktury += "║" + String.Format("{0,-78}", "Pozycja Faktury:") + "║\n";
            opisFaktury += "╠══════╦══════════════════╦═════════════╦════════════╦═══════════╦═════════════╣\n";
            opisFaktury += "║" + String.Format("{0,-5} {1,-18} {2,-10} {3,-10} {4,-10} {5,-10}", "Lp.", "║ Nazwa", "║ Kwota Netto", "║ Stawka VAT", "║ Kwota VAT", "║ Kwota Brutto") + "║\n";
            opisFaktury += "╠══════╬══════════════════╬═════════════╬════════════╬═══════════╬═════════════╣\n";
            opisFaktury += pozycja();
            // opisFaktury += "╠══════╬══════════════════╬═════════════╬════════════╬═══════════╬═════════════╣\n";
            opisFaktury += podsumowanie();
            opisFaktury += "╚══════╩══════════════════╩═════════════╩════════════╩═══════════╩═════════════╝\n";
            return opisFaktury;


        }
        public string pozycja()
        {
            var opisPozycji = "";
            foreach (var pozycjaFaktury in pozycje)
            {
                opisPozycji += "";

                opisPozycji +=String.Format("{0,-4} {1,-15} {2,4} {3,13} {4,12} {5,11} {6,9}",
                    pozycjaFaktury.lp, pozycjaFaktury.nazwa, "║", pozycjaFaktury.kwotaNetto,
                    String.Format("{0:F2} %", pozycjaFaktury.stawkaVat), String.Format("{0:N2}", (pozycjaFaktury.kwotaNetto * pozycjaFaktury.stawkaVat / 100)),
                    String.Format("{0:N2}", (pozycjaFaktury.kwotaNetto + (pozycjaFaktury.kwotaNetto * pozycjaFaktury.stawkaVat / 100))));


            }
            return opisPozycji;
        }

       
        
        public string podsumowanie()
        {
            var pods = "";
            pods += "";

            pods += "║ " + String.Format("{0,-4} {1,-15} {2,4} {3,13} {4,12} {5,11} {6,9}",
                "", "║ " + "RAZEM", "║", kwotaNetto() + " zł" + " ║", "" + " ║", String.Format("{0:N2}", kwotaVat()) + " zł" + " ║", String.Format("{0:N2}", kwotaBrutto())) + " zł" + "║\n";

            return pods;
        }

        public decimal kwotaNetto()
        {
            decimal suma = 0;
            foreach (var pozycjaFaktury in pozycje)
            {

                var kwotaNetto = pozycjaFaktury.kwotaNetto;
                suma += kwotaNetto;
            }
            return suma;
        }

        public decimal kwotaVat()
        {
            decimal sumaVat = 0;
            foreach (var pozycjaFaktury in pozycje)
            {

                var kwotaVat = (pozycjaFaktury.kwotaNetto * pozycjaFaktury.stawkaVat / 100);
                sumaVat += kwotaVat;
            }
            return sumaVat;
        }

        public decimal kwotaBrutto()
        {
            decimal suma = 0;
            foreach (var pozycjaFaktury in pozycje)
            {

                var kwotaBrutto = pozycjaFaktury.kwotaNetto + (pozycjaFaktury.kwotaNetto * pozycjaFaktury.stawkaVat / 100);
                suma += kwotaBrutto;
            }
            return suma;


        }
        public bool czyOpoznione()
        {
            if (dataPlatnosci.CompareTo(DateTime.Now) == -1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool obliczeniaNip()
        {
            var cyfra1 = numerNip / 1000000000;
            var cyfra2 = (numerNip / 100000000) % 10;
            var cyfra3 = (numerNip / 10000000) % 10;
            var cyfra4 = (numerNip / 1000000) % 10;
            var cyfra5 = (numerNip / 100000) % 10;
            var cyfra6 = (numerNip / 10000) % 10;
            var cyfra7 = (numerNip / 1000) % 10;
            var cyfra8 = (numerNip / 100) % 10;
            var cyfra9 = (numerNip / 10) % 10;
            var cyfra10 = numerNip % 10;

            var wynik = cyfra1 * 6 + cyfra2 * 5 + cyfra3 * 7 + cyfra4 * 2 + cyfra5 * 3 + cyfra6 * 4 + cyfra7 * 5 + cyfra8 * 6 + cyfra9 * 7;
            var wynik1 = wynik % 11;

            if (wynik1 == cyfra10)
            {
                return true;

            }
            else
            {
                return false;
            }

        }


    }
}