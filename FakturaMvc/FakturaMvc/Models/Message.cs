﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FakturaMvc.Models
{
    public class Message
    {
        public int typ;
        public string opis;

        public const int TYP_SUKCES = 1;
        public const int TYP_BLAD = 2;
        public const int TYP_OSTRZEZENIE = 3;
    }
}