﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FakturaMvc.Models
{
    public class Ksiazka
    {
        public int id { get; set; } = 0;
        public string tytul { get; set; } = string.Empty;
        public long isbn { get; set; } = 0;
        public decimal cena { get; set; } = 0;
        public string autor { get; set; } = string.Empty;

        public string cena2()
        //string cena3;
        //cena3 = String.Format("{0:F2} zł", cena);
        //return cena3;


        {

            return String.Format("{0:F2} zł", cena);
        }


        public string pobierzOpis()
        {
            var opisKsiazki = "";
            opisKsiazki += "Tytuł: " + tytul + ", ISBN: " + isbn + ", cena: " + String.Format("{0:F2} zł", cena) + ", autor: "
                + autor;
            if (czyPoprawny() == true)
            {
                opisKsiazki = opisKsiazki + ", ISBN poprawny";
            }
            else
            {
                opisKsiazki = opisKsiazki + ", ISBN niepoprawny";
            }
            return opisKsiazki;
        }
        public bool czyPoprawny()
        {
            var cyfra1 = isbn / 1000000000000;
            var cyfra2 = (isbn / 100000000000) % 10;
            var cyfra3 = (isbn / 10000000000) % 10;
            var cyfra4 = (isbn / 1000000000) % 10;
            var cyfra5 = (isbn / 100000000) % 10;
            var cyfra6 = (isbn / 10000000) % 10;
            var cyfra7 = (isbn / 1000000) % 10;
            var cyfra8 = (isbn / 100000) % 10;
            var cyfra9 = (isbn / 10000) % 10;
            var cyfra10 = (isbn / 1000) % 10;
            var cyfra11 = (isbn / 100) % 10;
            var cyfra12 = (isbn / 10) % 10;
            var cyfra13 = isbn % 10;

            var wynik = cyfra1 * 1 + cyfra2 * 3 + cyfra3 * 1 + cyfra4 * 3 + cyfra5 * 1
                + cyfra6 * 3 + cyfra7 * 1 + cyfra8 * 3 + cyfra9 * 1 + cyfra10 * 3
                + cyfra11 * 1 + cyfra12 * 3;
            var wynik1 = wynik % 10;
            var wynik2 = wynik1 % 10;
            if (wynik2 == cyfra13)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}